#sumowanie liczb w tablicy z wykorzystaniem reduce
def sum(numArr):
    return reduce(lambda x, y : x + y, numArr)


#wyznaczenie liczby maksymalnej
def max(numArr):
    return reduce(lambda a, b: a if (a > b) else b, numArr)


numbers = [11,200,31,53,82,97,121]

print "Suma wynosi: %s" % sum(numbers)

print "Max wynosi: %s" % max(numbers)